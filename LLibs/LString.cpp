﻿#include "stdafx.h"
#include "lstring.h"

#include <codecvt>

using std::string;
using std::wstring;
//
//LL::lstring::lstring(const std::basic_string& str, EEncoding encoding /*= EEncoding::UTF8*/) :
//	basic_lstring(str)
//{
//	if (encoding == EEncoding::ANSI)
//	{
//		assign(ansi_to_utf8(str));
//	}
//}

//LL::lstring::lstring(const _Myt& _Right, EEncoding encoding) :
//	basic_lstring(_Right)
//{
//	if (encoding == EEncoding::ANSI)
//	{
//		//assign(ansi_to_utf8(_Right));
//	}
//}

//LL::lstring::lstring(const char ch, EEncoding encoding /*= EEncoding::UTF8*/)
//{
//	utf8_str = encoding == EEncoding::ANSI ? ansi_to_utf8(lstring(ch)) : lstring(ch);
//}
//
//
//
//LL::lstring::lstring(const std::string& str, EEncoding encoding /*= UTF8*/)
//{
//	switch (encoding)
//	{
//	case EEncoding::ANSI:
//		utf8_str = ansi_to_utf8(str);
//		break;
//
//	case EEncoding::UTF8:
//		if (ensure(utf8::is_valid(str.begin(), str.end())))
//		{
//			utf8_str = str;
//		}
//		else
//		{
//			utf8::replace_invalid(str.begin(), str.end(), back_inserter(utf8_str));
//		}
//		break;
//
//	default:
//		break;
//	}
//}
//
//
//
//LL::lstring::lstring(const std::wstring& wstr, EEncoding encoding /*= UTF8*/)
//{
//	//switch (encoding)
//	//{
//	//case EEncoding::ANSI:
//	//	utf8_str = wide_to_utf8(wstr);
//	//	break;
//
//	//case EEncoding::UTF8:
//	//	if (ensure(utf8::is_valid(wstr.begin(), wstr.end())))
//	//	{
//	//		utf8_str = wstr;
//	//	}
//	//	else
//	//	{
//	//		utf8::replace_invalid(wstr.begin(), wstr.end(), back_inserter(utf8_str));
//	//	}
//	//	break;
//
//	//default:
//	//	break;
//	//}
//}
//
__int64 LL::lstring::length() const
{
	return utf8::distance(data.begin(), data.end());
}

std::string LL::lstring::to_ansi(const lstring& utf8_str)
{
	int size = MultiByteToWideChar(CP_UTF8, 0, utf8_str.c_str(),
		(int)utf8_str.data.length(), nullptr, 0);
	wstring utf16_str(size, '\0');
	MultiByteToWideChar(CP_UTF8, 0, utf8_str.c_str(),
		(int)utf8_str.data.length(), &utf16_str[0], (int)size);

	int ansi_size = WideCharToMultiByte(CP_ACP, 0, utf16_str.c_str(),
		(int)utf16_str.length(), nullptr, 0,
		nullptr, nullptr);

	std::string ansi_str(ansi_size, '\0');
	WideCharToMultiByte(CP_ACP, 0, utf16_str.c_str(),
		(int)utf16_str.length(), &ansi_str[0], ansi_size,
		nullptr, nullptr);

	return ansi_str;
}

//std::string LL::lstring::to_ansi(const string& utf8_str)
//{
//	int size = MultiByteToWideChar(CP_UTF8, 0, utf8_str.c_str(),
//		(int)utf8_str.length(), nullptr, 0);
//	wstring utf16_str(size, '\0');
//	MultiByteToWideChar(CP_UTF8, 0, utf8_str.c_str(),
//		(int)utf8_str.length(), &utf16_str[0], (int)size);
//
//	int ansi_size = WideCharToMultiByte(CP_ACP, 0, utf16_str.c_str(),
//		(int)utf16_str.length(), nullptr, 0,
//		nullptr, nullptr);
//	std::string ansi_str(ansi_size, '\0');
//	WideCharToMultiByte(CP_ACP, 0, utf16_str.c_str(),
//		(int)utf16_str.length(), &ansi_str[0], ansi_size,
//		nullptr, nullptr);
//	return ansi_str;
//}


std::wstring LL::lstring::to_ucs2(const lstring& utf8_str)
{
	std::wstring_convert<std::codecvt_utf8<wchar_t>> myconv;
	return myconv.from_bytes(utf8_str.c_str());
}


template<>
LL::basic_lstring LL::lstring::from_ansi<char>(const std::string& ansi_str)
{
	int size = MultiByteToWideChar(CP_ACP, 0, ansi_str.c_str(),
		(int)ansi_str.length(), nullptr, 0);
	wstring utf16_str(size, '\0');
	MultiByteToWideChar(CP_ACP, 0, ansi_str.c_str(),
		(int)ansi_str.length(), &utf16_str[0], size);

	int utf8_size = WideCharToMultiByte(CP_UTF8, 0, utf16_str.c_str(),
		(int)utf16_str.length(), nullptr, 0,
		nullptr, nullptr);

	basic_lstring utf8_str(utf8_size, '\0');
	WideCharToMultiByte(CP_UTF8, 0, utf16_str.c_str(),
		(int)utf16_str.length(), &utf8_str[0], utf8_size,
		nullptr, nullptr);

	return utf8_str;
}

template<>
LL::basic_lstring LL::lstring::from_ansi<wchar_t>(const std::wstring& ansi_str)
{
	int utf8_size = WideCharToMultiByte(CP_UTF8, 0, ansi_str.c_str(),
		(int)ansi_str.length(), nullptr, 0,
		nullptr, nullptr);

	basic_lstring utf8_str(utf8_size, '\0');
	WideCharToMultiByte(CP_UTF8, 0, ansi_str.c_str(),
		(int)ansi_str.length(), &utf8_str[0], utf8_size,
		nullptr, nullptr);

	return utf8_str;
}

template<>
LL::basic_lstring LL::lstring::from_ansi<char16_t>(const std::u16string& ansi_str)
{
	throw std::logic_error("The method or operation is not implemented.");
}

template<>
LL::basic_lstring LL::lstring::from_ansi<char32_t>(const std::u32string& ansi_str)
{
	throw std::logic_error("The method or operation is not implemented.");
}

LL::basic_lstring LL::lstring::from_ucs2(const std::wstring& ucs2_str)
{
	std::wstring_convert<std::codecvt_utf8<wchar_t>> myconv;
	return myconv.to_bytes(ucs2_str);
}
//
//std::wstring LL::lstring::utf8_to_ucs2(const lstring& utf8_str)
//{
//	std::wstring_convert<std::codecvt_utf8<wchar_t>> myconv;
//	return myconv.from_bytes(utf8_str.data.c_str());
//}
//
//std::string LL::lstring::ucs2_to_utf8(const std::wstring& wide_str)
//{
//	std::wstring_convert<std::codecvt_utf8<wchar_t>> myconv;
//	return myconv.to_bytes(wide_str);
//}








//LL::lstring::lstring(const char* str, EEncoding encoding /*= EEncoding::UTF8*/) :
//	basic_lstring(str)
//{
//	if (encoding == EEncoding::ANSI)
//	{
//		assign(ansi_to_utf8(str));
//	}
//}
//
//LL::lstring::lstring(const std::wstring& str, EEncoding encoding /*= EEncoding::UTF8*/)
//{
//	if (encoding == EEncoding::UTF8)
//	{
//		std::string temp = std::string(str.begin(), str.end());
//		//assign(lstring(temp.c_str()));
//		assign(temp.begin(),temp.end());
//	}
//	else
//	{
//		assign(ansi_to_utf8(str.c_str()));
//	}
//}