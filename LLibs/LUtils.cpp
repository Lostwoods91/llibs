﻿#include "stdafx.h"
#include "LUtils.h"

#include <strsafe.h>
#include <ShlObj.h>
#include <shellapi.h>
#include <time.h>

template <>
void LUtils::CreateDirectory<char>(const char* pszPath)
{
	int rc = SHCreateDirectoryExA(NULL, pszPath, NULL);
	ensureex(rc != ERROR_SUCCESS || rc != ERROR_FILE_EXISTS || rc != ERROR_ALREADY_EXISTS, "failed creating directory");
}

template <>
void LUtils::CreateDirectory<wchar_t>(const wchar_t* pszPath)
{
	int rc = SHCreateDirectoryExW(NULL, pszPath, NULL);
	ensureex(rc != ERROR_SUCCESS || rc != ERROR_FILE_EXISTS || rc != ERROR_ALREADY_EXISTS, "failed creating directory");
}

bool LUtils::DeleteFileOrDirectoryA(LPCSTR dir)
{
	CHAR szDir[MAX_PATH + 1];  // +1 for the double null terminate
	SHFILEOPSTRUCTA fos = { 0 };

	StringCchCopyA(szDir, MAX_PATH, dir);
	int len = lstrlenA(szDir);
	szDir[len + 1] = 0; // double null terminate for SHFileOperation

						// delete the folder and everything inside
	fos.wFunc = FO_DELETE;
	fos.pFrom = szDir;
	fos.fFlags = FOF_NO_UI;

	int rc = SHFileOperationA(&fos);
	if (rc == ERROR_FILE_NOT_FOUND) return false;
	if (!ensure(rc == ERROR_SUCCESS)) return false;
	return true;
}

bool LUtils::DeleteFileOrDirectoryW(LPCWSTR dir)
{
	WCHAR szDir[MAX_PATH + 1];  // +1 for the double null terminate
	SHFILEOPSTRUCTW fos = { 0 };

	StringCchCopyW(szDir, MAX_PATH, dir);
	int len = lstrlenW(szDir);
	szDir[len + 1] = 0; // double null terminate for SHFileOperation

						// delete the folder and everything inside
	fos.wFunc = FO_DELETE;
	fos.pFrom = szDir;
	fos.fFlags = FOF_NO_UI;

	int rc = SHFileOperationW(&fos);
	if (rc == ERROR_FILE_NOT_FOUND) return false;
	if (!ensure(rc == ERROR_SUCCESS)) return false;
	return true;
}

LONG LUtils::RenameFileOrDirectoryW(LPCWSTR From, LPCWSTR To)
{
	WCHAR szFrom[MAX_PATH + 1];  // +1 for the double null terminate
	WCHAR szTo[MAX_PATH + 1];  // +1 for the double null terminate
	SHFILEOPSTRUCTW fos = { 0 };

	StringCchCopyW(szFrom, MAX_PATH, From);
	int len = lstrlenW(szFrom);
	szFrom[len + 1] = 0; // double null terminate for SHFileOperation

	StringCchCopyW(szTo, MAX_PATH, To);
	len = lstrlenW(szTo);
	szTo[len + 1] = 0; // double null terminate for SHFileOperation

					   // delete the folder and everything inside
	fos.wFunc = FO_RENAME;
	fos.pFrom = szFrom;
	fos.pTo = szTo;
	fos.fFlags = FOF_NO_UI;

	return SHFileOperationW(&fos);
}

const char* LUtils::GetTimestamp()
{
	time_t t = time(NULL);
	struct tm buf;
	static char str[26];

	gmtime_s(&buf, &t);
	asctime_s(str, sizeof str, &buf);
	str[sizeof str - 2] = 0;
	//printf("UTC:   %s", str);

	//localtime_s(&buf, &t);
	//asctime_s(str, sizeof str, &buf);
	//printf("local: %s", str);

	return str;
}
