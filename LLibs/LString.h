﻿#pragma once

#include <string>

namespace LL
{
	typedef char lchar;
	typedef std::basic_string<lchar, std::char_traits<lchar>, std::allocator<lchar>> basic_lstring;

	enum class EEncoding
	{
		ANSI,
		UTF8
	};

	class lstring
	{
	private:
		basic_lstring data;
		mutable std::string ansi_str;
		mutable std::wstring ucs2_str;

	public:

		template<class charT>
		lstring(const charT ch, EEncoding enc = EEncoding::UTF8) :
			lstring(std::basic_string<charT>(1, ch), enc)
		{}

		template<class charT>
		lstring(const charT* str, EEncoding enc = EEncoding::UTF8) :
			lstring(std::basic_string<charT>(str), enc)
		{}

		template<class _Elem, class _Traits, class _Alloc>
		lstring(const std::basic_string<_Elem, _Traits, _Alloc>& str, EEncoding enc = EEncoding::UTF8)
			: data(enc == EEncoding::UTF8 ? basic_lstring(str.begin(), str.end()) : from_ansi(str))
		{}

		const lchar* c_str() const { return data.c_str(); }
		const lchar* operator*() const { return c_str(); }

		const char* to_ansi() const { ansi_str = to_ansi(data); return ansi_str.c_str(); }
		const wchar_t* to_ucs2() const { ucs2_str = to_ucs2(data); return ucs2_str.c_str(); }
		__int64 length() const;

		static std::string to_ansi(const lstring& utf8_str);
		static std::wstring to_ucs2(const lstring& utf8_str);

		template<class charT>
		static basic_lstring from_ansi(const std::basic_string<charT>& ansi_str);

		static basic_lstring from_ucs2(const std::wstring& ucs2_str);					// DA TESTARE!!!

		//static std::wstring utf8_to_ucs2(const lstring& utf8_str);
		//static std::string ucs2_to_utf8(const std::wstring& wide_str);

	};
};
