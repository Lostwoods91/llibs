// Test.cpp: definisce il punto di ingresso dell'applicazione console.
//

#include "stdafx.h"

#include "lstring.h"
#include <iostream>

using namespace LL;

// To avoid:
// warning C4566 : un carattere rappresentato dal nome di caratteri universali '\u03A0' non può essere rappresentato nella tabella codici corrente(1252)
// when using code like
// const char* pi = "Π";
// this file must be encoded in ANSI (use notepad++ to switch file encoding)
// PS: lines as
// char c1 = '\u03a0';
// will always produce this warning

const char* yA = "\u00FF";
const wchar_t* yA2 = L"\u00FF";
const char* y = "ÿ";
const wchar_t* yW = L"ÿ";
const char16_t* y16 = u"ÿ";
const char32_t* y32 = U"ÿ";

const char* piA = "\u03A0";
const wchar_t* piA2 = L"\u03A0";
const char* pi = "Π";
const wchar_t* piW = L"Π";
const char16_t* pi16 = u"Π";
const char32_t* pi32 = U"Π";

void test_containers()
{
	printf("%zd\n", sizeof(char));				// it prints: 1
	printf("%zd\n", sizeof(wchar_t));			// it prints: 2
	printf("%zd\n", sizeof(char16_t));			// it prints: 2
	printf("%zd\n", sizeof(char32_t));			// it prints: 4
}

void test_conversions()
{
	lstring ls1 = 'a';
	lstring ls2 = "a";
	lstring ls3 = L'a';
	lstring ls4 = L"a";
	lstring ls5 = u'a';
	lstring ls6 = u"a";
	lstring ls7 = U'a';
	lstring ls8 = U"a";

	{
		lstring lspi = pi;
		std::string ansi = lstring::to_ansi(lspi);				// not recognized char in code page
		std::wstring ucs2 = lstring::to_ucs2(lspi);				// ok!
	}

	{
		lstring lsy = y;
		std::string ansi = lstring::to_ansi(lsy);				// ok! correct code page
		std::wstring ucs2 = lstring::to_ucs2(lsy);				// ok!
	}

	{
		lstring lspi = lstring::from_ansi<char>(piA);			// invalid ansi code 
		std::string ansi = lspi.to_ansi();						// invalid ansi code
		lstring lspi2 = lstring::from_ucs2(piA2);				// ok!
		std::wstring ucs2 = lspi2.to_ucs2();					// ok!
	}

	{
		lstring lsy = lstring::from_ansi<char>(yA);				// ok!
		std::string ansi = lsy.to_ansi();						// ok!
		lstring lsy2 = lstring::from_ucs2(yA2);					// ok!
		std::wstring ucs2 = lsy2.to_ucs2();						// ok!
	}
}

int main()
{
	// Allow program to continue after an assertion has failed
	int prev = _set_error_mode(_OUT_TO_MSGBOX);

	lstring ls = pi;									// assume pi is utf8 encoded
	lstring ls2 = lstring(pi, EEncoding::ANSI);			// explicitly tell that pi is ansi encoded
	lstring lsW = piW;									// assume pi is utf8 encoded
	lstring ls2W = lstring(piW, EEncoding::ANSI);		// explicitly tell that pi is ansi encoded

	test_conversions();

	return 0;
}

void test()
{
	//const char* s = "ABçdè*°_£";
	//const char* watashi = "私";
	//const wchar_t* watashiW = L"私";
	//LString watashi_utf8(watashi, EEncoding::UTF8);
	//LString watashi_ansi(watashi, EEncoding::ANSI);
	//std::cout << watashi_utf8.to_ansi() << std::endl;
	//std::cout << watashi_ansi.to_ansi() << std::endl;
	//LString Str(s, EEncoding::UTF8);		// Setting encoding to utf8 triggers an assertion
	//std::string str = s;
	//std::cout << str << std::endl;
	//std::cout << Str.to_ansi() << std::endl;
	//std::wcout << Str.to_wide() << std::endl;
	//std::cout << *Str << std::endl;
	//std::cout << Str.length() << std::endl;
	//{
	//	LString LStr = L"µ0þ↨Y-Z¦Å▒&";
	//	std::string a = "µ0þ↨Y-☺Z¦Å▒&";
	//	const char* c = "µ0þ↨Y-Z¦Å▒&";
	//	std::cout << c << std::endl;
	//}
}
