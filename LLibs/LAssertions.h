#pragma once

#include <assert.h>

// implement verify
#ifdef _DEBUG
#define verify(f)          assert(f)
#else
#define verify(f)          ((void)(f))
#endif

namespace LL
{
	class Ensure
	{
	public:
		static bool ensureassert() { assert(0); return false; }
		static bool ensureex(const char* msg) { throw std::runtime_error(msg); return false; }
	};
}

// implement ensure
#define ensure(x) ((x) || LL::Ensure::ensureassert())

// implement ensure with exception

#define EXPAND( x ) x
#define GET_MACRO(_1,_2,NAME,...) NAME

#define ensureex1(x) ((x) || LL::Ensure::ensureex("ensure exception"))
#define ensureex2(x, msg) ((x) || LL::Ensure::ensureex(msg))

#define ensureex(...) EXPAND(GET_MACRO(__VA_ARGS__, ensureex2, ensureex1)(__VA_ARGS__))
