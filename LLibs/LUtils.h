﻿#pragma once

class LUtils
{
public:
	template <class charT>
	static void CreateDirectory(const charT* pszPath);

	static bool DeleteFileOrDirectoryA(LPCSTR dir);
	static bool DeleteFileOrDirectoryW(LPCWSTR dir);

	static LONG RenameFileOrDirectoryW(LPCWSTR From, LPCWSTR To);

	static const char* GetTimestamp();
};

