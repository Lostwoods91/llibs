﻿#pragma once

#include <vector>

// https://isocpp.org/wiki/faq/templates#separate-template-fn-defn-from-decl

template <class charT>
using LString = std::basic_string<charT>;

class LPaths
{

private:
	
	template <class charT>
	static const charT* strchr(const charT* _String, charT _Ch);

	template <class charT>
	static const charT* strrchr(const charT* _String, charT _Ch);

	template <class charT, class FIND_DATA_T>
	static HANDLE FindFirstFile(const charT* hFindFile, FIND_DATA_T* lpFindFileData);

	template <class FIND_DATA_T>
	static BOOL FindNextFile(HANDLE hFindFile, FIND_DATA_T* lpFindFileData);

	template <class charT, class FIND_DATA_T>
	static void CollectFilesRecursivelyPrivate(const charT* DirFullpath, std::vector<LString<charT>>& Files);

public:

	template <class charT>
	static int _access(const charT* Path, int mode);

	template <class charT>
	static DWORD GetFileAttributes(const charT* lpFileName);

	template <class charT>
	static void AutocompleteWithFinalSlash(LString<charT>& str);

	template <class charT>
	static void RemoveFinalSlash(LString<charT>& str);

	template <class charT>
	static void SetPathSlashes(LString<charT>& str, bool ToUseStraightSlashes);

	template <class charT>
	static void NormalizePath(LString<charT>& str, bool ToUseBackSlashes, bool ToUseFinalSlash);

	template <class charT>
	static void SurroundWith(LString<charT>& str, charT ch);

	template <class charT>
	static LString<charT> GetFilename(const LString<charT>& str);

	template <class charT>
	static LString<charT> GetExtension(const LString<charT>& str);

	template <class charT>
	static LString<charT> GetFilenameWithoutExtension(const LString<charT>& str);

	template <class charT>
	static LString<charT> GetDirectoryPath(const LString<charT>& str);

	template <class charT>
	static void CollectFilesRecursively(const charT* DirFullpath, std::vector<LString<charT>>& Files);

	static std::wstring S2WS(std::string s);
	static std::string WS2S(std::wstring ws);
};

