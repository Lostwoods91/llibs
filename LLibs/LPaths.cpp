﻿#include "stdafx.h"
#include "LPaths.h"

#include <corecrt_io.h>

using std::wstring;
using std::string;

template<>
const char* LPaths::strchr<char>(const char* _String, char _Ch)
{
	return ::strchr(_String, _Ch);
}

template<>
const wchar_t* LPaths::strchr<wchar_t>(const wchar_t* _String, wchar_t _Ch)
{
	return ::wcschr(_String, _Ch);
}

template<>
const char* LPaths::strrchr<char>(const char* _String, char _Ch)
{
	return ::strrchr(_String, _Ch);
}

template<>
const wchar_t* LPaths::strrchr<wchar_t>(const wchar_t* _String, wchar_t _Ch)
{
	return ::wcsrchr(_String, _Ch);
}

template <>
HANDLE LPaths::FindFirstFile<char, WIN32_FIND_DATAA>(LPCSTR lpFileName, LPWIN32_FIND_DATAA lpFindFileData)
{
	return ::FindFirstFileA(lpFileName, lpFindFileData);
}

template <>
HANDLE LPaths::FindFirstFile<wchar_t, WIN32_FIND_DATAW>(LPCWSTR lpFileName, LPWIN32_FIND_DATAW lpFindFileData)
{
	return ::FindFirstFileW(lpFileName, lpFindFileData);
}

template <>
BOOL LPaths::FindNextFile<WIN32_FIND_DATAA>(HANDLE hFindFile, LPWIN32_FIND_DATAA lpFindFileData)
{
	return ::FindNextFileA(hFindFile, lpFindFileData);
}

template <>
BOOL LPaths::FindNextFile<WIN32_FIND_DATAW>(HANDLE hFindFile, LPWIN32_FIND_DATAW lpFindFileData)
{
	return ::FindNextFileW(hFindFile, lpFindFileData);
}


template <class charT, class FIND_DATA_T>
void LPaths::CollectFilesRecursivelyPrivate(const charT* DirFullpath, std::vector<LString<charT>>& Files)
{
	LString<charT> DirFullpathStr(DirFullpath);
	NormalizePath(DirFullpathStr, true, true);
	LString<charT> SearchPattern = DirFullpathStr;
	SearchPattern += '*';

	FIND_DATA_T FindData;
	HANDLE hFind = FindFirstFile<charT, FIND_DATA_T>(SearchPattern.c_str(), &FindData);
	if (hFind != INVALID_HANDLE_VALUE)
	{
		// Skip . and .. directories
		FindNextFile(hFind, &FindData);
		while (FindNextFile(hFind, &FindData))
		{
			if (FindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				LString<charT> SubDir = DirFullpathStr;
				SubDir += FindData.cFileName;
				SubDir += '\\';
				CollectFilesRecursivelyPrivate<charT, FIND_DATA_T>(SubDir.c_str(), Files);
			}
			else
			{
				LString<charT> File = DirFullpathStr;
				File += FindData.cFileName;
				Files.push_back(File);
			}
		}
	}
	FindClose(hFind);
}

template <>
int LPaths::_access<char>(const char* _FileName, int _AccessMode)
{
	return ::_access(_FileName, _AccessMode);
}

template <>
int LPaths::_access<wchar_t>(const wchar_t* _FileName, int _AccessMode)
{
	return ::_waccess(_FileName, _AccessMode);
}

template <>
DWORD LPaths::GetFileAttributes<char>(const char* lpFileName)
{
	return ::GetFileAttributesA(lpFileName);
}

template <>
DWORD LPaths::GetFileAttributes<wchar_t>(const wchar_t* lpFileName)
{
	return ::GetFileAttributesW(lpFileName);
}

template <class charT>
void LPaths::AutocompleteWithFinalSlash(LString<charT>& str)
{
	if (str.size() > 0)
	{
		charT lastchar = str.at(str.length() - 1);
		if (lastchar == '\\' || lastchar == '/')
		{
			return;
		}
		const charT* reverseslash = strchr<charT>(str.c_str(), '\\');
		if (reverseslash != 0)
		{
			str += '\\';
		}
		else
		{
			str += '/';
		}
	}
}
template void LPaths::AutocompleteWithFinalSlash<char>(std::string& str);
template void LPaths::AutocompleteWithFinalSlash<wchar_t>(std::wstring& str);

template <class charT>
void LPaths::RemoveFinalSlash(LString<charT>& str)
{
	if (str.length() == 0) return;
	charT lastchar = str.at(str.length() - 1);
	if (lastchar == '\\' || lastchar == '/')
	{
		str = str.substr(0, str.length() - 1);
	}
}
template void LPaths::RemoveFinalSlash<char>(std::string& str);
template void LPaths::RemoveFinalSlash<wchar_t>(std::wstring& str);

template <class charT>
void LPaths::SetPathSlashes(LString<charT>& str, bool ToUseStraightSlashes)
{
	if (ToUseStraightSlashes)
	{
		int i = 0;
		while (str[i] > 0)
		{
			if (str[i] == '\\') str[i] = '/';
			i++;
		}
	}
	else
	{
		int i = 0;
		while (str[i] > 0)
		{
			if (str[i] == '/') str[i] = '\\';
			i++;
		}
	}
}
template void LPaths::SetPathSlashes<char>(std::string& str, bool ToUseStraightSlashes);
template void LPaths::SetPathSlashes<wchar_t>(std::wstring& str, bool ToUseStraightSlashes);

template <class charT>
void LPaths::NormalizePath(LString<charT>& str, bool ToUseBackSlashes, bool ToUseFinalSlash)
{
	const charT* ReverseSlash = strchr<charT>(str.c_str(), '\\');
	bool IsBackSlashUsed = ReverseSlash != 0;
	const charT* StraightSlash = strchr<charT>(str.c_str(), '/');
	bool IsStraightSlashUsed = StraightSlash != 0;

	if (ToUseBackSlashes && IsStraightSlashUsed)				// double check to manage mixed cases
	{
		SetPathSlashes(str, false);
	}
	else if (!ToUseBackSlashes && IsBackSlashUsed)
	{
		SetPathSlashes(str, true);
	}

	if (ToUseFinalSlash)
	{
		AutocompleteWithFinalSlash(str);
	}
	else
	{
		RemoveFinalSlash(str);
	}
}
template void LPaths::NormalizePath<char>(std::string& str, bool ToUseBackSlashes, bool ToUseFinalSlash);
template void LPaths::NormalizePath<wchar_t>(std::wstring& str, bool ToUseBackSlashes, bool ToUseFinalSlash);

template <class charT>
void LPaths::SurroundWith(LString<charT>& str, charT ch)
{
	str = ch + str + ch;
}
template void LPaths::SurroundWith<char>(std::string& str, char ch);
template void LPaths::SurroundWith<wchar_t>(std::wstring& str, wchar_t ch);

template <class charT>
LString<charT> LPaths::GetFilename(const LString<charT>& str)
{
	size_t pos = str.rfind('\\');
	if (pos == LString<charT>::npos)
	{
		pos = str.rfind('/');
	}
	if (pos == LString<charT>::npos)
	{
		return str;
	}
	else
	{
		return str.substr(pos + 1);
	}
}
template std::string LPaths::GetFilename<char>(const std::string& str);
template std::wstring LPaths::GetFilename<wchar_t>(const std::wstring& str);

template <class charT>
LString<charT> LPaths::GetExtension(const LString<charT>& str)
{
	size_t pos = str.rfind('.');
	if (pos == LString<charT>::npos)
	{
		return LString<charT>();
	}
	else
	{
		return str.substr(pos + 1);
	}
}
template std::string LPaths::GetExtension<char>(const std::string& str);
template std::wstring LPaths::GetExtension<wchar_t>(const std::wstring& str);

template <class charT>
LString<charT> LPaths::GetFilenameWithoutExtension(const LString<charT>& str)
{
	LString<charT> ris = GetFilename(str);
	size_t pos = ris.rfind('.');
	if (pos == LString<charT>::npos)
	{
		return ris;
	}
	else
	{
		return ris.substr(0, pos);
	}
}
template std::string LPaths::GetFilenameWithoutExtension<char>(const std::string& str);
template std::wstring LPaths::GetFilenameWithoutExtension<wchar_t>(const std::wstring& str);

template <class charT>
LString<charT> LPaths::GetDirectoryPath(const LString<charT>& str)
{
	size_t pos = str.rfind('\\');
	if (pos == LString<charT>::npos)
	{
		pos = str.rfind('/');
	}
	if (pos == LString<charT>::npos)
	{
		return LString<charT>();
	}
	else
	{
		return str.substr(0, pos + 1);
	}
}
template std::string LPaths::GetDirectoryPath<char>(const std::string& str);
template std::wstring LPaths::GetDirectoryPath<wchar_t>(const std::wstring& str);

template <>
void LPaths::CollectFilesRecursively<char>(const char* DirFullpath, std::vector<string>& Files)
{
	CollectFilesRecursivelyPrivate<char, WIN32_FIND_DATAA>(DirFullpath, Files);
}

template <>
void LPaths::CollectFilesRecursively<wchar_t>(const wchar_t* DirFullpath, std::vector<wstring>& Files)
{
	CollectFilesRecursivelyPrivate<wchar_t, WIN32_FIND_DATAW>(DirFullpath, Files);
}

wstring LPaths::S2WS(string s)
{
	return wstring(s.begin(), s.end());
}

string LPaths::WS2S(wstring ws)
{
	return string(ws.begin(), ws.end());
}